#!/usr/bin/env php
<?php

require(__DIR__.'/vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare('logs', 'fanout', false, false, false);

echo '--START--'.PHP_EOL;
echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$callback = function($msg) {
    echo " [x] Received ", $msg->body, "\n";
};

$channel->basic_consume('tasks', sha1(mt_rand()), false, true, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}