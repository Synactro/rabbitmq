<?php

require(__DIR__.'/vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare('tasks', false, false, false, false);

$i=10;

while($i--) {

    $message = json_encode(array('title' => 'Traitement du message N'.$i.'"'.sha1(mt_rand()).'"', 'duration' => rand(1, 10)));
    $amqpMessage = new AMQPMessage($message);

    echo $i.'<br>';
    $channel->basic_publish($amqpMessage, '', 'tasks');
//    sleep(1);
}

$channel->close();
$connection->close();